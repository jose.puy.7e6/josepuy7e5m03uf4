﻿using System;

namespace JosePuy7e5M03UF4
{

    class FiguraGeometrica
    {
        protected int codi { get; set; }

        protected string nom { get; set; }

        protected ConsoleColor color { get; set; }
    }

    class Rectangle
    {
        // Definición de las propiedades de la clase indicada.
        public double baseR { get; set; }

        public double heightR { get; set; }

        // Constructor predeterminado.
        public Rectangle()
        {
            baseR = 2;

            heightR = 4;
        }

        // Constructor con parametros modificables.
        public Rectangle(double baseR, double heightR)
        {
            this.baseR = baseR;

            this.heightR = heightR;
        }

        // Constructor que crea a base de otro objeto de la misma clase.
        public Rectangle(Rectangle r)
        {
            baseR = r.baseR;

            heightR = r.heightR;
        }

        // Funcion que retorna la información del objeto.
        public string toString() => "La base del rectangle és " + this.baseR + " i l'altura és " + this.heightR;

        // Funcion que retorna el perimetro del objeto si es calculable.
        public string Perimetre()
        {
            double perimetre = this.baseR * 2 + this.heightR * 2;
            if (perimetre < 0)
            {
                return "El perimetre no es calculable";
            }
            else
            {
                return "El perimetre del rectangle es " + perimetre;
            }
        }

        // Funcion que devuelve el Area del objeto si es calculable.
        public string Area()
        {
            double area = this.baseR * this.heightR;
            if (area < 0)
            {
                return "L'area no es calculable";
            }
            else
            {
                return "L'area del rectangle es " + area;
            }
        }
    }

    class Triangle
    {
        public double baseT { get; set; }

        public double heightT { get; set; }

        public Triangle()
        {
            baseT = 2;

            heightT = 4;
        }

        public Triangle(double newBase, double newHeight)
        {
            this.baseT = newBase;

            this.heightT = newHeight;
        }

        public Triangle(Triangle t)
        {
            baseT = t.baseT;

            heightT = t.heightT;
        }

        public string toString() => "La base del triangle és " + this.baseT + " i l'altura és " + this.heightT;

        public string Perimetre()
        {
            double perimetre = this.baseT + this.heightT * 2;
            if (perimetre < 0)
            {
                return "El perimetre no es calculable";
            }
            else
            {
                return "El perimetre del triangle es " + perimetre;
            }
        }

        public string Area()
        {
            double area = (this.baseT * this.heightT) / 2;
            if (area < 0)
            {
                return "L'area no es calculable";
            }
            else
            {
                return "L'area del triangle es " + area;
            }
        }
    }

    class Cercle
    {
        public double radius { get; set; }

        public Cercle()
        {
            radius = 4;
        }

        public Cercle(double radius)
        {

        }

        public Cercle(Cercle c)
        {
            radius = c.radius;
        }

        public string toString() => "El radi del cercle és " + this.radius;

        public string Perimetre()
        {
            double perimetre = Math.PI * (this.radius * 2);
            if (perimetre < 0)
            {
                return "El perimetre no es calculable";
            }
            else
            {
                return "El perimetre del cercle és " + perimetre;
            }

        }

        public string Area()
        {
            double area = Math.Pow(Math.PI * this.radius, 2);
            if (area < 0)
            {
                return "L'area no es calculable";
            }
            else
            {
                return "El permietre del triangle és " + area;
            }
        }
    }

    internal class ProvaFigures
    {
        static void Main()
        {
            Rectangle prueba = new Rectangle();
            Console.WriteLine(prueba.baseR);
            prueba.baseR = 2.32;
            Console.WriteLine(prueba.baseR);

            Console.WriteLine(prueba.Area());
            Console.WriteLine(prueba.Perimetre());

        }
    }
}
